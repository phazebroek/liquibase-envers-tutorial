update rockstar
set tribe =
        (select name
         from tribe
         where tribe.id = rockstar.tribe_id);

update rockstar
set tribe_id = null;

update rockstar
set updated_by =
        (select rockstar_log.updated_by
         from rockstar_log
                  left join rockstar
                            on rockstar_log.id = rockstar.id
         order by rev desc
         limit 1);

update rockstar
set updated_at =
        (select rockstar_log.updated_at
         from rockstar_log
                  left join rockstar
                            on rockstar_log.id = rockstar.id
         order by rev desc
         limit 1);
