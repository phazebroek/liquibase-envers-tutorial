insert into tribe (name, created_by)
    (select distinct r.tribe, 'liquibase:create-tribes:phazebroek'
     from rockstar as r
     where r.tribe is not null);

insert into tribe_log (id, rev, revtype, name, created_by, created_at)
    (select t.id, create_rev(), 1, t.name, t.created_by, t.created_at
     from tribe as t);
