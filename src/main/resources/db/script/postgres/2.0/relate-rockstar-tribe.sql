update rockstar
set tribe_id =
        (select id
         from tribe
         where tribe.name = rockstar.tribe
           and rockstar.tribe is not null);

update rockstar
set updated_by = 'liquibase:relate-rockstar-tribe:phazebroek';

update rockstar
set updated_at = (select CURRENT_TIMESTAMP);

insert into rockstar_log (id, rev, revtype, firstname, lastname, tribe, tribe_id, created_by, created_at, updated_by, updated_at)
    (select r.id,
            create_rev(),
            1,
            r.firstname,
            r.lastname,
            r.tribe,
            r.tribe_id,
            r.created_by,
            r.created_at,
            r.updated_by,
            r.updated_at
     from rockstar as r);