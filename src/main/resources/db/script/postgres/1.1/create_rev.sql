-- Adds a new rev to the revinfo table and returns the rev number for use when inserting a new record in an audit table.
create or replace function create_rev() returns integer
    language sql
as
$$
    insert into revinfo (rev, revtstmp)
    values (nextval('hibernate_sequence'), (select extract(epoch from CURRENT_TIMESTAMP) * 1000))
    returning rev
$$;
