-- Update the audit table with the records that were added by the loadData change.
-- the rev is populated using the create_rev function.
-- revtype 0 means insert
-- the created_by is used to only select those records that were added as part of the related changeset.
insert into rockstar_log (id,
                          rev,
                          revtype,
                          firstname,
                          lastname,
                          tribe,
                          created_by,
                          created_at)
    (select r.id,
            create_rev(),
            0, -- 0 for insert
            r.firstname,
            r.lastname,
            r.tribe,
            r.created_by,
            r.created_at
     from rockstar as r
     where r.created_by = 'liquibase:add-default-rockstars:phazebroek');