package nl.phazebroek.poc.tribe;

import java.util.List;
import javax.validation.Valid;
import lombok.RequiredArgsConstructor;
import nl.phazebroek.poc.tribe.Tribe;
import nl.phazebroek.poc.tribe.TribeRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

/**
 * @since 2.0
 */
@RequiredArgsConstructor
@RestController
@RequestMapping("tribes")
public class TribeController {

    private final TribeRepository tribeRepository;

    @GetMapping
    public ResponseEntity<List<Tribe>> getTribes() {
        return ResponseEntity.ok(tribeRepository.findAll());
    }

    @GetMapping("{id}")
    public ResponseEntity<Tribe> getTribe(@PathVariable("id") int id) {
        var tribe = tribeRepository.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tribe not found"));
        return ResponseEntity.ok(tribe);
    }

    @PostMapping
    public ResponseEntity<Void> addTribe(@Valid @RequestBody Tribe tribe) {
        tribe.setId(null);
        tribeRepository.save(tribe);
        return ResponseEntity.noContent().build();
    }

    @PutMapping("{id}")
    public ResponseEntity<Void> updateTribe(@PathVariable("id") int id, @Valid @RequestBody Tribe tribe) {
        tribe.setId(id);
        tribeRepository.save(tribe);
        return ResponseEntity.noContent().build();
    }

    @DeleteMapping("{id}")
    public ResponseEntity<Void> deleteTribe(@PathVariable("id") int id) {
        var tribe = tribeRepository.findById(id)
            .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND, "Tribe not found"));
        tribeRepository.delete(tribe);
        return ResponseEntity.noContent().build();
    }
}
