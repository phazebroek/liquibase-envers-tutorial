# liquibase-envers-tutorial

A tutorial using Liquibase and Hibernate Envers for auditing and managing a database.

## Getting started

The `main` branch contains the full solution.

To experiment with this project, start with `release/1.0` branch. 

Run PostgresQL using the provided docker-compose file.
Once PostgresQL is up, run the app with:

`./mvnw clean verify spring-boot:run`

In the `http` directory are some requests that can be used with IntelliJ's built-in HTTP client to fiddle with the app.

Stop the app, checkout the next release and start the app again. Watch the database gets updated automatically.

To rollback (one changeset at a time), use the liquibase-maven-plugin:

`./mvnw clean verify liquibase:rollback -DrollbackCount=1`

Note: rolling back beyond a single changelog may cause unexpected results.